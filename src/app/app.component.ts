import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from './redux/reducers';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(public store: Store<AppState>) {
    }

    ngOnInit(): void {

    }

}
