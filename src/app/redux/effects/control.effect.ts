import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {HttpClient} from '@angular/common/http';
import {map, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {
    ControlActionTypes, RunRemoveQueryControl, RunRemoveQueryControlFailed, RunRemoveQueryControlSuccess
} from '../actions/control.action';
import {ControlService} from '../../service/control.service';

@Injectable()
export class AdminEffects {

    @Effect({dispatch: true})
    runRemoveQueryControl$ = this.actions$.pipe(
        ofType(ControlActionTypes.RUN_REMOVE_QUERY_CONTROL),
        switchMap((action: RunRemoveQueryControl) => this.adminService.removeControls(action.payload)
            .pipe(
                map(response => {
                        console.log('response ' + JSON.stringify(response));
                        if (response !== null) {
                            return new RunRemoveQueryControlSuccess(response);
                        } else {
                            return new RunRemoveQueryControlFailed(response);
                        }
                    }
                )
            )
        ));

    constructor(private actions$: Actions, private http: HttpClient, private router: Router,
                private adminService: ControlService) {
    }
}
