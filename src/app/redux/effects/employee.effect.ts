import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {HttpClient} from '@angular/common/http';
import {map, switchMap, tap} from 'rxjs/operators';
import {UserService} from '../../service/user.service';
import {Router} from '@angular/router';
import {
    EmployeeActionTypes,
    UpdateEmployee, UpdateEmployeeFailed, UpdateEmployeeSuccess,
    UserSignIn,
    UserSignInFailed,
    UserSignInSuccess
} from '../actions/employee.action';

@Injectable()
export class LoginEffects {

    @Effect({dispatch: true})
    login$ = this.actions$.pipe(
        ofType(EmployeeActionTypes.EMPLOYEE_SIGN_IN),
        switchMap((action: UserSignIn) => this.userService.login(action.payload)
            .pipe(
                map(response => {
                        console.log('response ' + JSON.stringify(response));
                        if (response === true) {
                            return new UserSignInSuccess(action.payload);
                        } else {
                            return new UserSignInFailed(response);
                        }
                    }
                )
            )
        ));

    @Effect({dispatch: true})
    userSignInSuccess$ = this.actions$.pipe(
        ofType(EmployeeActionTypes.EMPLOYEE_SIGN_IN_SUCCESSFUL),
        switchMap((action: UserSignInSuccess) => this.userService.updateEmployee(action.payload)
            .pipe(
                map(response => {
                        console.log('response ' + JSON.stringify(response));
                        if (response === null || response === undefined) {
                            return new UpdateEmployeeFailed(response);
                        } else {
                            return new UpdateEmployeeSuccess(response);
                        }
                    }
                )
            )
        ));
    @Effect({dispatch: false})
    updateEmployeeSuccess$ = this.actions$.pipe(
        ofType(EmployeeActionTypes.UPDATE_EMPLOYEE_SUCCESSFUL),
        tap(() => this.router.navigateByUrl('Home'))
    );

    /*
    @Effect({dispatch: true})
    logout$ = this.actions$
        .pipe(
            ofType(EmployeeActionTypes.USER_SIGN_OUT),
            switchMap((action: UserSignOut) => this.userService.login(action.payload)
                .pipe(
                    map(response => {
                            console.log('response ' + response);
                            if (response === true) {
                                return new UserSignOutSuccess(action.payload)
                            } else {
                                return new UserSignOutFailed(response)
                            }
                        }
                    )
                )
            ));
    */
    @Effect({dispatch: false})
    logOutSuccess$ = this.actions$.pipe(
        ofType(EmployeeActionTypes.EMPLOYEE_SIGN_OUT),
        tap(() => this.router.navigateByUrl('Login'))
    );

    constructor(private actions$: Actions, private http: HttpClient, private router: Router,
                private userService: UserService) {
    }
}
