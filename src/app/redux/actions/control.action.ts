import {Action} from '@ngrx/store';


export const ControlActionTypes = {
    RUN_REMOVE_QUERY_CONTROL: '[ADMIN] = RUN REMOVE QUERY CONTROL',
    RUN_REMOVE_QUERY_CONTROL_SUCCESSFUL: '[ADMIN] = RAN REMOVE QUERY CONTROL WAS SUCCESSFUL',
    RUN_REMOVE_QUERY_CONTROL_FAILED: '[ADMIN] = RAN REMOVE QUERY CONTROL FAILED',

    RUN_ADD_QUERY_CONTROL: '[ADMIN] = RUN ADD QUERY CONTROL',
    RUN_ADD_QUERY_CONTROL_SUCCESSFUL: '[ADMIN] = RAN ADDING QUERY CONTROL WAS SUCCESSFUL',
    RUN_ADD_QUERY_CONTROL_FAILED: '[ADMIN] = RAN ADDING QUERY CONTROL FAILED',

};


export class RunRemoveQueryControl implements Action {
    readonly type = ControlActionTypes.RUN_REMOVE_QUERY_CONTROL;

    constructor(public payload: any) {

    }
}

export class RunRemoveQueryControlSuccess implements Action {
    readonly type = ControlActionTypes.RUN_REMOVE_QUERY_CONTROL;

    constructor(public payload: any) {

    }
}

export class RunRemoveQueryControlFailed implements Action {
    readonly type = ControlActionTypes.RUN_REMOVE_QUERY_CONTROL;

    constructor(public payload: any) {

    }
}

