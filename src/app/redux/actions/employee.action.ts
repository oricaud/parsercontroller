import {Action} from '@ngrx/store';
import {Employee} from '../../model/interfaces/employee';

export const EmployeeActionTypes = {
    EMPLOYEE_SIGN_IN: '[EMPLOYEE] = SIGN IN',
    EMPLOYEE_SIGN_IN_SUCCESSFUL: '[EMPLOYEE] = SIGN IN WAS SUCCESSFUL',
    EMPLOYEE_SIGN_IN_FAILED: '[EMPLOYEE] = SIGN IN FAILED',
    EMPLOYEE_SIGN_OUT: '[EMPLOYEE] = SIGN OUT',
    EMPLOYEE_SIGN_OUT_SUCCESSFUL: '[EMPLOYEE] = SIGN OUT WAS SUCCESSFUL',
    EMPLOYEE_SIGN_OUT_FAILED: '[EMPLOYEE] = SIGN OUT FAILED',
    UPDATE_EMPLOYEE: '[EMPLOYEE] = UPDATE EMPLOYEE',
    UPDATE_EMPLOYEE_SUCCESSFUL: '[EMPLOYEE] = UPDATE EMPLOYEE WAS SUCCESSFUL',
    UPDATE_EMPLOYEE_FAILED: '[EMPLOYEE] = UPDATE EMPLOYEE FAILED',
};


export class UpdateEmployee implements Action {
    readonly type = EmployeeActionTypes.UPDATE_EMPLOYEE;

    constructor(public payload: Employee) {

    }
}

export class UpdateEmployeeSuccess implements Action {
    readonly type = EmployeeActionTypes.UPDATE_EMPLOYEE_SUCCESSFUL;

    constructor(public payload: Employee) {

    }
}

export class UpdateEmployeeFailed implements Action {
    readonly type = EmployeeActionTypes.UPDATE_EMPLOYEE_FAILED;

    constructor(public payload: Employee) {

    }
}
export class UserSignIn implements Action {
    readonly type = EmployeeActionTypes.EMPLOYEE_SIGN_IN;

    constructor(public payload: Employee) {

    }
}

export class UserSignInSuccess implements Action {
    readonly type = EmployeeActionTypes.EMPLOYEE_SIGN_IN_SUCCESSFUL;

    constructor(public payload: any) {

    }
}

export class UserSignInFailed implements Action {
    readonly type = EmployeeActionTypes.EMPLOYEE_SIGN_IN_FAILED;

    constructor(public payload: any) {

    }
}

export class UserSignOut implements Action {
    readonly type = EmployeeActionTypes.EMPLOYEE_SIGN_OUT;

    constructor(public payload: any) {

    }
}

export class UserSignOutSuccess implements Action {
    readonly type = EmployeeActionTypes.EMPLOYEE_SIGN_OUT_SUCCESSFUL;

    constructor(public payload: any) {

    }
}

export class UserSignOutFailed implements Action {
    readonly type = EmployeeActionTypes.EMPLOYEE_SIGN_OUT_FAILED;

    constructor(public payload: any) {

    }
}
