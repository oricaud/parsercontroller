import {EmployeeActionTypes} from '../actions/employee.action';
import {Action} from '@ngrx/store';
import {Employee} from '../../model/interfaces/employee';

export interface EmployeeAppState {
    user: Employee;
    error: any;
    loading: any;
    authenticated: boolean;
}

const initialLoginState: EmployeeAppState = {
    user: null,
    error: false,
    loading: false,
    authenticated: false,
};


export function employeeReducer(employeeState = initialLoginState, action: Action): EmployeeAppState {

    switch (action.type) {
        case EmployeeActionTypes.EMPLOYEE_SIGN_IN:
            return {
                ...employeeState,
                user: action.payload,
                authenticated: false,
            };
        case EmployeeActionTypes.EMPLOYEE_SIGN_IN_SUCCESSFUL:
            return {
                ...employeeState,
                // user: action['payload'],
                error: false,
                authenticated: true,
            };
        case EmployeeActionTypes.EMPLOYEE_SIGN_IN_FAILED:
            return {
                ...employeeState,
                error: true,
                authenticated: false,
            };
        case EmployeeActionTypes.UPDATE_EMPLOYEE:
            console.log('updateEmplyee ' + JSON.stringify(action.payload));
            return {
                ...employeeState,
                user: action.payload,
                error: false,
                authenticated: true,
            };
        case EmployeeActionTypes.UPDATE_EMPLOYEE_SUCCESSFUL:
            console.log('updateEmplyee ' + JSON.stringify(action.payload));
            return {
                ...employeeState,
                user: action.payload,
                error: false,
                authenticated: true,
            };
        case EmployeeActionTypes.EMPLOYEE_SIGN_OUT:
            return {
                ...employeeState,
                user: null,
                authenticated: true,
            };
        case EmployeeActionTypes.EMPLOYEE_SIGN_OUT_SUCCESSFUL:
            return {
                ...employeeState,
                user: null,
                authenticated: true,
            };

        case EmployeeActionTypes.EMPLOYEE_SIGN_OUT_FAILED:
            return {
                ...employeeState,
                user: null
            };


        default:
            return employeeState;
    }
}
