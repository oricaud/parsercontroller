import {ActionReducer, ActionReducerMap, createSelector, MetaReducer} from '@ngrx/store';
import {EmployeeAppState, employeeReducer} from './employee.reducer';
// Import the necessary methods for saving and loading
import {storageSync} from '@larscom/ngrx-store-storagesync';
import {ControlAppState, controlReducer} from "./control.reducer";


/* Overrides the complaint Property payload does not exist on type Action */
declare module '@ngrx/store' {
    interface Action {
        type: string;
        payload?: any;
    }
}

export interface AppState { /* Use this as the place to store your interfaces to the store */
    employee: EmployeeAppState;
    control: ControlAppState;
}

export const reducers: ActionReducerMap<AppState> = {
    employee: employeeReducer,
    control: controlReducer,
};

export function getState(state: AppState): AppState {
    return state;
}

export function fetchUser(state: AppState): EmployeeAppState { /* Fetches the interface state */
    return state.employee;
}


export const getUser = createSelector(getState, fetchUser);

export function storageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return storageSync<EmployeeAppState>({
        features: [
            // saves the login state to sessionStorage
            {stateKey: 'employee'}
        ],
        storage: window.sessionStorage
    })(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [storageSyncReducer];
