import {Action} from '@ngrx/store';
import {ControlActionTypes} from '../actions/control.action';
import {Control} from "../../model/interfaces/control";

export interface ControlAppState {
    control: Control;
}

const initialLoginState: ControlAppState = {
    control: null,
};


export function controlReducer(adminAppState = initialLoginState, action: Action): ControlAppState {

    switch (action.type) {


        case ControlActionTypes.RUN_REMOVE_QUERY_CONTROL:
            return {
                ...adminAppState,
                control: action.payload
            };
        case ControlActionTypes.RUN_REMOVE_QUERY_CONTROL_SUCCESSFUL:
            return {
                ...adminAppState,
                control: action.payload
            };
        case ControlActionTypes.RUN_REMOVE_QUERY_CONTROL_FAILED:
            return {
                ...adminAppState,
                control: null
            };

        default:
            return adminAppState;
    }
}
