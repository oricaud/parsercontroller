import {NgModule} from '@angular/core';
import {DefaultUrlSerializer, RouterModule, Routes, UrlSerializer, UrlTree} from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {ParserComponent} from "./components/parser/parser.component";

export class LowerCaseUrlSerializer extends DefaultUrlSerializer {
    parse(url: string): UrlTree {
        return super.parse(url.toLowerCase());
    }
}

const appRoutes: Routes = [
    {
        path: '', component: ParserComponent
    },
    {
        path: 'parser', component: ParserComponent
    },
];


@NgModule({
    declarations: [],
    imports: [BrowserModule, HttpClientModule, RouterModule.forRoot(appRoutes)],
    exports: [RouterModule],
    providers: [
        {
            provide: UrlSerializer,
            useClass: LowerCaseUrlSerializer
        }
    ],
})
export class AppRoutingModule {
}
