import {Component, OnInit} from '@angular/core';
import { RunRemoveQueryControl} from "../../redux/actions/control.action";
import {AppState} from "../../redux/reducers";
import {Store} from "@ngrx/store";

@Component({
    selector: 'app-parser',
    templateUrl: './parser.component.html',
    styleUrls: ['./parser.component.css']
})
export class ParserComponent implements OnInit {

    public results: string[] = [];
    public numOfResults = 0;

    constructor(public store: Store<AppState>) {}

    ngOnInit() {}

    textAreaUpdate(input: string) {
        console.log('input ' + input);
        this.results = this.generateParameters(input);
        console.table(this.results);
    }

    generateParameters(params: string) {
        let tokens: string[];

        params = params.replace(/\s+/g, " ");

        if (params.includes("\n")) {
            params = params.replace("\n", ",");
            tokens = params.split("\n");
        } else {
            tokens = params.split(/[, ]/gm);
        }

        console.log( JSON.stringify(tokens) );

        return this.validateTokens(tokens);
    }

    validateTokens(tokens: string[]) {
        let numOfZeroes = 0;
        let cleanTokens:string[] = [];

        for (let i = 0; i < tokens.length; i++) {
            if (tokens[i].length < 8 && tokens[i].length > 0) {
                numOfZeroes = 8 - tokens[i].length;

                for (let j = 0; j < numOfZeroes; j++) {
                    tokens[i] = '0' + tokens[i];
                }
                cleanTokens.push(tokens[i]);
                this.numOfResults++;

            } else if(tokens[i].length > 0 && tokens[i].length < 9){
                cleanTokens.push(tokens[i]);
                this.numOfResults++;
            }
        }

        return cleanTokens;
    }

    printTokens(tokens: string[]){
        for(let i = 0; i < tokens.length; i++){
            console.log(tokens[i]);
        }
    }

    runQuery(control: string[]) {
        this.store.dispatch(new RunRemoveQueryControl(control));
    }

}
