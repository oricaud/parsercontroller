import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {metaReducers, reducers} from './redux/reducers';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {EffectsModule} from '@ngrx/effects';
import {ServiceWorkerModule} from '@angular/service-worker';
import {LoginEffects} from './redux/effects/employee.effect';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatNativeDateModule, MatProgressSpinnerModule} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgReduxModule} from '@angular-redux/store';
import {DemoMaterialModule} from '../material-module';
import {AdminEffects} from './redux/effects/control.effect';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ParserComponent } from './components/parser/parser.component';


@NgModule({
    declarations: [
        AppComponent,
        ParserComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        DemoMaterialModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        MatNativeDateModule,
        NgReduxModule,
        StoreModule.forRoot(reducers, {metaReducers}),
        StoreDevtoolsModule.instrument({maxAge: 5, name: 'App Security'}),
        EffectsModule.forRoot([LoginEffects, AdminEffects]),
        ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
        NgbModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
