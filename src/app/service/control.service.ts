import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Control} from "../model/interfaces/control";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'
    })
};

@Injectable({
    providedIn: 'root'
})

export class ControlService {

    private baseUrl = 'http://localhost:8092'; // End point

    constructor(private httpClient: HttpClient) {
    }


    /* Method is responsible for removing an array of controls from the database on the back end */
    removeControls(control: Control[]): Observable<Control> {
        return this.httpClient.post<Control>(this.baseUrl + '/removeControls', control, httpOptions).pipe(catchError(this.handleError));
    }

    /* Method is responsible for removing an array of controls from the database on the back end */
    addControls(control: Control[]): Observable<Control> {
        return this.httpClient.post<Control>(this.baseUrl + '/addControls', control, httpOptions).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${JSON.stringify(error.status)}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }
}
