import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Employee} from '../model/interfaces/employee';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'
    })
};

@Injectable({
    providedIn: 'root'
})

export class UserService {
    private isAuthenticated = false;

    private baseUrl = 'http://localhost:8092'; // End point

    constructor(private httpClient: HttpClient) {
    }


    /* Method is responsible for adding a transaction to the database on the back end */
    login(employee: Employee): Observable<Employee> {
        return this.httpClient.post<Employee>(this.baseUrl + '/login', employee, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    updateEmployee(employee: Employee): Observable<Employee> {
        console.log('updateEmployee ' + JSON.stringify(employee));
        return this.httpClient.post<Employee>(this.baseUrl + '/updateEmployee', employee, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    /**
     * Determines if the user is authenticated
     */
    public authenticated(): Observable<boolean> {
        return of(this.isAuthenticated);
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${JSON.stringify(error.status)}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }
}
