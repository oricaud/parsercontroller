export class Employee {
    id?: number;
    firstName?: string;
    lastName?: string;
    role?: string;
    address?: string;
    city?: string;
    state?: string;
    country?: string;
    zip?: string;
    createId?: string;
    createDt?: any;
    updateId?: string;
    updateDt?: any;
    username?: string;
    password?: string;
}
