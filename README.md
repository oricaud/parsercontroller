# ParseWeb
@author Oscar I. Ricaud - Systems Dev
@author Adrian Hurtado  - Systems Dev

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.

## Development server

Open terminal and run `cd/parse-app` then run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

## Offline 
You have to do two steps! First build the application for production then install http-server if you don't have it installed.
## Step 1
Merely build the application on path `src/main/web/src` then run the `ng build --prod`
## Step 2
A new directory should be created named `dist`
Go to this directory `demo\src\main\web\my-app\dist` then run `http-server -p 8082 -c-1 my-app` 
### Step 3
Test the offline mode by going to the google chrome going to the network tab and clicking on the `offline` mode. Refresh the page 
And bam chickitabam the web application still runs

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Redux
We use for the state management Redux. Please view the Redux documentation `https://ngrx.io/api`
